import socket
import random

# Define the top 30 Ferengi Rules of Acquisition
rules_of_acquisition = [
    "Once you have their money, you never give it back.",
    "The best deal is the one that makes the most profit.",
    "A Ferengi without profit is no Ferengi at all.",
    "Keep your ears open.",
    "Keep your eyes on the stars and your feet on the ground.",
    "Opportunity plus instinct equals profit.",
    "Greed is eternal.",
    "Anything worth doing is worth doing for money.",
    "A deal is a deal... until a better one comes along.",
    "Free advice is seldom cheap.",
    "Never allow family to stand in the way of opportunity.",
    "You can't make a deal if you're dead.",
    "Wives serve, brothers inherit.",
    "Only fools pay retail.",
    "There's nothing more dangerous than an honest businessman.",
    "Let others keep their reputation. You keep their money.",
    "Never cheat a Klingon... unless you can get away with it.",
    "It's always good business to know about new customers before they walk in your door.",
    "The justification for profit is profit.",
    "New customers are like razor-toothed greeworms. They can be succulent, but sometimes they bite back.",
    "Employees are the rungs on the ladder of success. Don't hesitate to step on them.",
    "Never begin a business negotiation on an empty stomach.",
    "The riskier the road, the greater the profit.",
    "Home is where the heart is, but the stars are made of latinum.",
    "Every once in a while, declare peace. It confuses the hell out of your enemies.",
    "Beware of the Vulcan greed for knowledge.",
    "The flimsier the product, the higher the price.",
    "Never let the competition know what you're thinking."
]

# Server configuration
host = '0.0.0.0'  # Listen on all network interfaces
port = 6259  # Example port

# Setup socket
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((host, port))
    s.listen()
    print(f"Service listening on port {port}...")

    while True:
        conn, addr = s.accept()
        with conn:
            print(f"Connected by {addr}")
            rule = random.choice(rules_of_acquisition)
            conn.sendall(rule.encode('utf-8'))
            conn.close()
