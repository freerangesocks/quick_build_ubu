#!/bin/bash

# Update system
sudo apt-get update && sudo apt-get upgrade -y

# Install Apache, MySQL, PHP (LAMP stack)
sudo apt-get install apache2 mysql-server php libapache2-mod-php php-mysql -y

# Secure MySQL installation
sudo mysql_secure_installation

# Enable mod_rewrite, necessary for some applications
sudo a2enmod rewrite
sudo systemctl restart apache2

# Download and install DVWA
cd /var/www/html
sudo git clone https://github.com/digininja/DVWA.git
sudo mv DVWA dvwa
sudo chown -R www-data:www-data /var/www/html/dvwa
sudo chmod -R 755 /var/www/html/dvwa

# Configure DVWA
sudo cp /var/www/html/dvwa/config/config.inc.php.dist /var/www/html/dvwa/config/config.inc.php
# Note: You should manually update the database connection settings in /var/www/html/dvwa/config/config.inc.php

# Restart Apache to apply changes
sudo systemctl restart apache2

# Firewall setup: Allow HTTP and other insecure ports you want to simulate
sudo ufw allow in "Apache"
sudo ufw allow 21  # FTP (Example)
sudo ufw allow 23  # Telnet (Example)
sudo ufw enable

# Install Python3 and pip
sudo apt-get install python3 python3-pip -y

echo "Installation and basic setup completed. Important next steps:"
echo "- Secure your MySQL installation."
echo "- Configure DVWA and other applications with specific settings."
echo "- Ensure this environment is isolated and used only for educational purposes."
echo "- Regularly update your server and applications."
echo "- Manually open or close ports as per your educational curriculum needs."
